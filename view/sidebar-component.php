<div class="col-12 seo-dashboard" data-editor="<?= $editor ?? '' ?>">
    <?php
    $options = get_option('ask_serp_settings');
    $req = null;
    if(isset($req['body'])) {
	 $req = json_decode($req['body'],true);
    $req ??= json_decode(get_post_meta(get_the_ID(), 'askSerpPostJson', true), true);
	
}
	 //var_dump($req['data']);
    if (empty($req)) {
        ?>
        <h5>
            Search Settings
        </h5>
        <div>
            <ul>
				<li><strong>Query: </strong><input type="text" placeholder="Type your preferred search query" id="searchQuery" name="search_query"/></li>
                <li><strong>Serp API: </strong><?= $options['serp_key'] ? 'Set' : 'Unset' ?></li>
                <li><strong>From Location: </strong><?= $options['serp_location'] ?? 'Unset' ?></li>
                <li><strong>Domain: </strong><?= $options['serp_domain'] ?? 'Unset' ?></li>
                <li><strong>Device: </strong><?= $options['serp_device'] ?? 'Unset' ?></li>
            </ul>
            <p>
                The query comes directly from your post title. Everything else comes from your E2E Serp Settings. <a
                        href="/wp-admin/admin.php?page=settings" target="_blank">Click here</a> to update.
            </p>
        </div>
        <div class="serp-button-holder">
            <?php
            $canQuery = !empty($options['serp_key']) && !empty($options['serp_location']);
            ?>
            <button type="button" class="refresh-data button button-primary" <?= $canQuery ? '' : 'disabled' ?>>
                <?= $canQuery ? 'Run Ask Serp' : 'Settings Required' ?>
            </button>
            <p>

                The SERP plugin will utilise your post title and find the best possible information needed to ensure
                your content is supremely optimised for rankings.
            </p>
        </div>
        <?php
    } else {
	
	//var_dump(json_decode($req['body'],true));
	


    ?>
    <input type="hidden" name="serp-post-data"
           value='<?= json_encode($req, JSON_THROW_ON_ERROR | JSON_HEX_QUOT | JSON_HEX_APOS) ?>'/>
	<?php $req = $req['data'];
		var_dump($req);
	    $avgWords = $req['response']['averageWordCount'];
    $minWords = $avgWords * 1.2;
    $maxWords = $avgWords * 1.5;
	?>
    <input type="hidden" name="serp-post-score"/>
    <div class="gauge-wrapper">
        <div class="gauge four rischio3">
            <div class="slice-colors">
                <div class="st slice-item"></div>
                <div class="st slice-item"></div>
                <div class="st slice-item"></div>
                <div class="st slice-item"></div>
            </div>
            <div class="needle"></div>
            <div class="gauge-center">
                <div class="label">Score</div>
                <div class="number"></div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col">
            <div class="card">
                <div class="card-title">Words</div>
                <span class="card-value" id="wordcount"></span>
                <span class="card-muted" id="wordestimate" data-val='<?= $avgWords ?>'>(<?= number_format(
                        $minWords,
                        0
                    ) . '-' . number_format($maxWords, 0) ?>)</span>
            </div>
        </div>
        <div class="col">
            <div class="card">
                <div class="card-title">Characters</div>
                <span class="card-value" id="charactercount"></span>
                <span class="card-muted" id="characterestimate" data-val='<?= $avgWords * 6 ?>'>(<?= number_format(
                        $minWords * 6,
                        0
                    ) . '-' . number_format($maxWords * 6, 0) ?>)</span>
            </div>
        </div>
        <div class="col">
            <div class="card">
                <div class="card-title">Paragraphs</div>
                <span class="card-value" id="paragraphcount"></span>
                <span class="card-muted" id="paragraphestimate" data-val='<?= $avgWords / 150 ?>'>(<?= number_format(
                        $minWords / 150,
                        0
                    ) . '-' . number_format($maxWords / 150, 0) ?>)</span>
            </div>
        </div>
        <div class="col">
            <div class="card">
                <div class="card-title">Internal Links</div>
                <span class="card-value" id="internalcount"></span>
                <span class="card-muted" id="internalestimate" data-val='<?= $avgWords / 400 ?>'>(<?= number_format(
                        $minWords / 400,
                        0
                    ) . '-' . number_format($maxWords / 400, 0) ?>)</span>
            </div>
        </div>
    </div>

    <div class="">

        <div class="tab">
            <button class="tablinks active" onclick="openTab(event, 'keywords')">Keywords</button>
            <button class="tablinks" onclick="openTab(event, 'headings')">Headings</button>
            <button class="tablinks" onclick="openTab(event, 'seo')">SEO</button>
            <button class="tablinks" onclick="openTab(event, 'serp')">SERP</button>
            <button class="tablinks" onclick="openTab(event,'settings')">
                <svg width="24" height="24" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" role="img"
                     aria-hidden="true" focusable="false">
                    <path fill-rule="evenodd"
                          d="M10.289 4.836A1 1 0 0111.275 4h1.306a1 1 0 01.987.836l.244 1.466c.787.26 1.503.679 2.108 1.218l1.393-.522a1 1 0 011.216.437l.653 1.13a1 1 0 01-.23 1.273l-1.148.944a6.025 6.025 0 010 2.435l1.149.946a1 1 0 01.23 1.272l-.653 1.13a1 1 0 01-1.216.437l-1.394-.522c-.605.54-1.32.958-2.108 1.218l-.244 1.466a1 1 0 01-.987.836h-1.306a1 1 0 01-.986-.836l-.244-1.466a5.995 5.995 0 01-2.108-1.218l-1.394.522a1 1 0 01-1.217-.436l-.653-1.131a1 1 0 01.23-1.272l1.149-.946a6.026 6.026 0 010-2.435l-1.148-.944a1 1 0 01-.23-1.272l.653-1.131a1 1 0 011.217-.437l1.393.522a5.994 5.994 0 012.108-1.218l.244-1.466zM14.929 12a3 3 0 11-6 0 3 3 0 016 0z"
                          clip-rule="evenodd"></path>
                </svg>
            </button>
        </div>

        <div id="keywords" class="tabcontent" style="display: block">
            <div class="tag-wrapper keywords">
                <?php
                foreach ($req['response']['keywordList'] as $w) { ?>
                    <div class="tag-holder">
                        <div class="tag-text"><?= $w['phrase'] ?></div>
                        <div class="tag-value"><span class="tag-count"></span> / <span class="tag-required"
                                                                                       data-value="<?= $w['density'] ?>"></span>
                        </div>
                    </div>
                    <?php
                } ?>
            </div>
        </div>
		

        <div id="headings" class="tabcontent">
            <div class="tag-wrapper headings">
                <?php
                foreach ($phrases = ['H1', 'H2', 'H3', 'H4'] as $h) { ?>
                    <div class="tag-holder">
                        <div class="tag-text"><?= $h ?></div>
                        <div class="tag-value" data-heading-attr="<?= $h ?>"><span class="tag-count"></span> / <span
                                    class="tag-required"></span></div>
                    </div>
                    <?php
                } ?>
            </div>
        </div>
		
		<div id="seo" class="tabcontent">
			<div class="serp-accordion">
				<h4 class="accordion-toggle">
					<div>Basic SEO</div>
					<svg width="24" height="24" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"
						 class="components-panel__arrow" role="img" aria-hidden="true" focusable="false">
						<path d="M17.5 11.6L12 16l-5.5-4.4.9-1.2L12 14l4.5-3.6 1 1.2z"></path>
					</svg>
				</h4>
				<div class="accordion-content">
					<ul>
						<li>Section Coming soon...</li>
					</ul>
				</div>
			</div>
        </div>

        <div id="serp" class="tabcontent">
            <div class="serp-accordion">
                <?php
                if (!empty($req['response']['relatedQuestionList'])) { ?>
                    <h4 class="accordion-toggle">
                        <div>Related Questions</div>
                        <svg width="24" height="24" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"
                             class="components-panel__arrow" role="img" aria-hidden="true" focusable="false">
                            <path d="M17.5 11.6L12 16l-5.5-4.4.9-1.2L12 14l4.5-3.6 1 1.2z"></path>
                        </svg>
                    </h4>
                    <div class="accordion-content">
                        <ul>
                            <?php
                            foreach ($req['response']['relatedQuestionList'] as $x) { ?>
                                <li><?= $x ?></li>
                            <?php
                            } ?>
                        </ul>
                    </div>
                <?php
                } ?>

                <?php
                if (!empty($req['response']['relatedSearchList'])) { ?>
                    <h4 class="accordion-toggle">
                        <div>
                            Related Searches
                        </div>
                        <svg width="24" height="24" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"
                             class="components-panel__arrow" role="img" aria-hidden="true" focusable="false">
                            <path d="M17.5 11.6L12 16l-5.5-4.4.9-1.2L12 14l4.5-3.6 1 1.2z"></path>
                        </svg>
                    </h4>
                    <div class="accordion-content">
                        <ul>
                            <?php
                            foreach ($req['response']['relatedSearchList'] as $x) { ?>
                                <li><?= $x ?></li>
                            <?php
                            } ?>
                        </ul>
                    </div>
                <?php
                } ?>

                <?php
                if (!empty($req['response']['searchResultList'])) { ?>
                    <h4 class="accordion-toggle">
                        <div>Top Search Results</div>
                        <svg width="24" height="24" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"
                             class="components-panel__arrow" role="img" aria-hidden="true" focusable="false">
                            <path d="M17.5 11.6L12 16l-5.5-4.4.9-1.2L12 14l4.5-3.6 1 1.2z"></path>
                        </svg>
                    </h4>
                    <div class="accordion-content">
                        <ul>
                            <?php
                            foreach ($req['response']['searchResultList'] as $x) { ?>
                                <li><a href="<?= $x['uri'] ?>" target="_blank"><?= $x['title'] ?></a></li>
                            <?php
                            } ?>
                        </ul>
                    </div>
                <?php
                } ?>
            </div>
        </div>

        <div id="settings" class="tabcontent">
            <div>
                <ul>
					
				
                    <li><strong>Query: </strong><input type="text" placeholder="Type your preferred search query" value="<?= $req['request']['query'] ?>" id="searchQuery" name="search_query"/></li>
                    <li><strong>From Location: </strong><?= $req['request']['location'] ?></li>
                    <li><strong>Domain: </strong><?= $req['request']['domain'] ?></li>
                    <li><strong>Device: </strong><?= $req['request']['device'] ?></li>
                </ul>
                <p>
                    The query comes directly from your post title. Everything else comes from your Ask Serp Settings. <a
                            href="/wp-admin/admin.php?page=settings" target="_blank">Click here</a> to update.
                </p>
            </div>
            <div class="serp-button-holder">
                <button type="button" class="refresh-data button button-primary">Pull SERP Data</button>
                <p>Only pull if needed, there is a charge per request.</p>
            </div>
        </div>
    </div>
        <?php
    } ?>
</div>
