const editor = jQuery('.seo-dashboard').attr('data-editor');
/*
if(editor === 'blocks') {
	wp.data.subscribe(function () {
    if (false == !jQuery('.block-editor-block-list__layout').html() || false == !jQuery('.wp-editor-container').html()) {
    updateText();
}
});
}

if(editor === 'classic') {

	setTimeout(function() {
		tinymce.activeEditor.on('NodeChange keyup', function() {
		updateText();

});
	},100);
	
}
*/
function moveNav() {
setTimeout(function () {
    jQuery('.components-panel').prepend(jQuery('#ask-serp-analyser').closest('.edit-post-meta-boxes-area'));
    updateText();
}, 100);
}

moveNav();

jQuery('body').on('click', '.edit-post-sidebar__panel-tab', function () {
moveNav();
});

function updateText() {
	let textArea;
	if(editor === 'blocks') {
		textArea = jQuery('.block-editor-block-list__layout').html();
	}
	if(editor === 'classic') {
		textArea = tinymce.activeEditor.getContent();
	}
const val = textArea.toLowerCase().replace(/(<([^>]+)>)/gi, " ");
	const valHtml = textArea.toLowerCase();

const wc = val.split(' ').length;
const cc = val.length;
	function formatNumber(value) {
		return Intl.NumberFormat('en-GB').format(value);
	}

jQuery("#wordcount").text(formatNumber(wc));
jQuery("#charactercount").text(formatNumber(cc));
let totalReq = 0;
let totalKey = 0;

jQuery("#keywords .tag-text").each(function () {
    let count = val.split(jQuery(this).html()).length - 1;
    req = Math.ceil(wc * jQuery(this).siblings('.tag-value').find('.tag-required').attr('data-value'));
    totalReq = totalReq + req;
    totalKey = totalKey + (req < count ? req : count);
    
    
    let minReq = Math.floor(req * 0.7);
    let maxReq = Math.ceil(req);
    
    jQuery(this).siblings('.tag-value').find('.tag-count').html(count);
    jQuery(this).siblings('.tag-value').find('.tag-required').html(minReq + '-'+maxReq);
    
    
    let newClass = count == 0 ? '' : count >= minReq && count <= maxReq ? 'green' : 'amber';
            
    jQuery(this).closest('.tag-holder').removeClass(['green','amber']).addClass(newClass);
            
});

let paragraphs = count = valHtml.split('<p').length - 1;
jQuery('#paragraphcount').html(paragraphs);
	
let siteURL = "https://" + top.location.host.toString();
paragraphs = count = valHtml.split('href="'+siteURL).length - 1 ;
	
let internalLinks = jQuery(".block-editor-block-list__layout a[href^='"+siteURL+"'], .block-editor-block-list__layout a[href^='/'], .block-editor-block-list__layout a[href^='./'], .block-editor-block-list__layout a[href^='../'], .block-editor-block-list__layout a[href^='#']").length;
jQuery('#internalcount').html(internalLinks);

let perKeywords = totalKey / totalReq;
let words = jQuery('#wordcount').html().replace(',','');
let perWords = words / jQuery('#wordestimate').attr('data-val');
let perParagraphs = jQuery('#paragraphcount').html() / jQuery('#paragraphestimate').attr('data-val');

let percentage = (perKeywords > 1 ? 1 : perKeywords * 70) + (perWords > 1 ? 1 : perWords * 20) + (perParagraphs > 1 ? 1 : perParagraphs * 10);
let degree = Math.floor(percentage/100 * 180);
percentage = percentage.toFixed(2);



jQuery('.needle').css('transform', 'rotate(' + degree + 'deg)');

jQuery('.gauge-center').find('.number').html(percentage + '%');
jQuery('[name=serp-post-score]').val(percentage + '%');

jQuery("#headings .tag-text").each(function () {
    let heading = jQuery(this).siblings('.tag-value').attr('data-heading-attr');
    v = jQuery(this).siblings('.tag-value');
    if (heading === 'H1') {
        minReq = 1;
        maxReq = 1;
        count = 1
        v.find('.tag-required').html('1');
        v.find('.tag-count').html(count);
    } else if (heading === 'H2') {
        req = words / 150;
        minReq = Math.floor(req * 0.8);
        maxReq = Math.ceil(req * 1.2);
        count = valHtml.split('<h2').length - 1;
    } else if (heading === 'H3') {
        req = words / 100;
        minReq = Math.floor(req * 0.8);
        maxReq = Math.ceil(req * 1.2);
        count = valHtml.split('<h3').length - 1;
    }
    
        v.find('.tag-count').html(count);
        v.find('.tag-required').html(minReq + '-'+maxReq);
    
    
    let newClass = count == 0 ? '' : count >= minReq && count <= maxReq ? 'green' : 'amber';
            
    jQuery(this).closest('.tag-holder').removeClass(['green','amber']).addClass(newClass);
    
});

}

function openTab(evt, tabName) {
var i, tabcontent, tablinks;
tabcontent = document.getElementsByClassName("tabcontent");
for (i = 0; i < tabcontent.length; i++) {
    tabcontent[i].style.display = "none";
}
tablinks = document.getElementsByClassName("tablinks");
for (i = 0; i < tablinks.length; i++) {
    tablinks[i].className = tablinks[i].className.replace(" active", "");
}
document.getElementById(tabName).style.display = "block";
evt.currentTarget.className += " active";
}


jQuery('body').on('click', '.refresh-data', function () {
let postTitle = jQuery('#searchQuery').val();
	if(postTitle == '') {
		alert('You need a title first!');
	} else {
jQuery('.refresh-data').html('Loading...').prop('disabled',true);
ask_serp_query({q: postTitle}).then((text) => {
    if ('' === text) {
        jQuery('.refresh-data').html('Try Again...').prop('disabled',false);
        jQuery('.refresh-data').click();
        return;
    } else if(text.startsWith('Error')) {
		alert(text);
		jQuery('.refresh-data').html('Try Again...').prop('disabled',false);
		return;
	}

    jQuery('#ask-serp-analyser .inside').html(text);
	updateText();
});
	}
});

jQuery('body').on('click', '.accordion-toggle', function () {
	jQuery(this).toggleClass('active');
jQuery(this).next().slideToggle('fast');
  jQuery(".accordion-content").not(jQuery(this).next()).slideUp('fast');

});
