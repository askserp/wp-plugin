<?php

$key = get_option('ask_serp_settings');

add_action('admin_menu', 'ask_setup_menu');

function add_subs($sub) {
    add_submenu_page(
        $sub['slug'],
        $sub['page_title'],
        $sub['menu_title'],
        $sub['capability'],
        $sub['menu_slug'],
        function() use ($sub) {
        require_once (plugin_dir_path(__FILE__) . 'view/settings/'.$sub['menu_slug'].'.php');
    });
}


function ask_setup_menu()
{
    add_menu_page('Ask Serp', 'Ask Serp', 'manage_options', 'askserp',function() {
        require_once (plugin_dir_path(__FILE__) . 'view/settings/index.php');
        wp_enqueue_script('custom-js', plugins_url('/assets/js/custom.js', __FILE__));
        wp_enqueue_style('custom-css', plugins_url('/assets/css/custom.css', __FILE__));
    },'data:image/svg+xml;base64,' . base64_encode('<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="32px" height="32px" viewBox="0 0 32 32" enable-background="new 0 0 32 32" xml:space="preserve">  <image id="image0" width="32" height="32" x="0" y="0" xlink:href="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACoAAAAgCAYAAABkWOo9AAAAAXNSR0IArs4c6QAAAYxJREFUWEfdl0FOAzEMRWNp2JQDsGZ6oR4AiVOwHk1PUokTcKCWfdfddURQKgVFaWJ/OymodJvEfv7xfKfkDL/zPHrp2MN0IGmPZt0U7C5AEcig1J8r2gMUiTGs6Ine9sfYHqqrRxLEwCVFNefzOL8GaoFMYWFQbaJUUe3Zkhs0gQaYGkQE7QEZYkGgHEwLaM0Z0phxjxlUUoxTG7GvAJsWIoJaFWsFzfv0f4BKaoaqLf3LeW1t/rOK3hoU6VVxMiGQrYqm6pHzH8P0uVEr2gN0mdev3vmd5jlXs6zi1XMmnQeSCrIYfglWDYqq0zpCc9grUIsCJfgeryfW8G8JGgtCc1RB0QDI9SMvfCnffYOe5/HknHtE+026RkRRzofzYfDzMWksKS1GsiepTdC8F9Bl+/ziPb1r1ZQmUy/IEOcCilZVSmw9q/mQRFBydBim/bqnMt47WrbjFxezOJmsiiB9KhVYW4dHqCWBdJVoTNWjBA2a72uF5SxN/CuihbbAIp7bHVQaBHEdgUtF+gaEtzDNdDc9nwAAAABJRU5ErkJggg==" />
</svg>'));

    $subMenu = [
        /*[
            'slug' => 'askserp',
            'page_title' => 'Robots.txt',
            'menu_title' => 'Robots.txt',
            'capability' => 'manage_options',
            'menu_slug' => 'robots'
        ]*/
    ];

    foreach($subMenu as $sub) {
        add_subs($sub);
    }

}




add_action('admin_init', 'ask_settings_init');
function ask_settings_init()
{
    register_setting('askPlugin', 'ask_serp_settings');

    add_settings_section(
        'ask_plugin_section',
        'Ask Serp Management',
        null,
        'askSerp'
    );
}



add_action('admin_footer', 'ask_admin_ajax_js');
function ask_admin_ajax_js()
{ ?>
    <script type="text/javascript">
        function ask_serp_query({q}) {
            const body = new FormData();
            body.append('action', 'ask_serp_query');
            body.append('q', q);

            return fetch(ajaxurl, {
                method: 'POST',
                body,
            }).then((response) => response.text());
        }
    </script>
    <?php
}

add_action('wp_ajax_ask_serp_query', 'ask_serp_query');
function ask_serp_query()
{
    $options = get_option('ask_serp_settings');

    $url = 'https://api.askserp.com/v1/query';

    $q = http_build_query([
        'key' => $options['serp_key'],
        'query' => $_POST['q'],
        'country' => $options['serp_country'],
        'language' => $options['serp_language'],
        'device' => $options['serp_device'],
        'domain' => $options['serp_domain'],
        'location' => $options['serp_location']
    ]);

	try {
    	$req = wp_remote_get("{$url}?{$q}", ['headers' => ['accept'=>'application/json']]);
        //var_dump($req);wp_die();
    	$body = json_decode($req['body'], true, 512, JSON_THROW_ON_ERROR);
		
		if(!$body['success']) {
			echo 'Error: '.$body['err'];
			wp_die();
		}

    	if (4 === $body['data']['status']) {
    		include plugin_dir_path(__FILE__) . '/../view/sidebar-component.php';
		}
	} catch(Throwable $ex) {
		echo $ex->getMessage();
	}
    wp_die();
}
