<form action='options.php' method='post'>
    <h2>General Settings</h2>
<div class="serp-accordion">

            <?php
                $key = get_option('ask_serp_settings');
                settings_fields('askPlugin');
                do_settings_sections('askPlugin');

                $serpKey = isset($key['serp_key']) ? $key['serp_key'] : null;
                $serpLocation = isset($key['serp_location']) ? $key['serp_location'] : null;
                $serpDevice = isset($key['serp_device']) ? $key['serp_device'] : null;
                $serpDomain = isset($key['serp_domain']) ? $key['serp_domain'] : null;
                $serpCountry = isset($key['serp_country']) ? $key['serp_country'] : null;
                $serpLanguage = isset($key['serp_language']) ? $key['serp_language'] : null;
                $serpPages = isset($key['serp_on_pages']) ? $key['serp_on_pages'] : null;
                $serpPosts = isset($key['serp_on_posts']) ? $key['serp_on_posts'] : null;
            ?>
        <h4 class="accordion-toggle">
            <div>Ask Serp</div>
            <svg width="24" height="24" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"
                 class="components-panel__arrow" role="img" aria-hidden="true" focusable="false">
                <path d="M17.5 11.6L12 16l-5.5-4.4.9-1.2L12 14l4.5-3.6 1 1.2z"></path>
            </svg>
        </h4>

        <div class="accordion-content" style="display: block">
    <table class="form-table" role="presentation">
        <tbody>
        <tr>
            <th scope="row">AskSerp Licence Key</th>
            <td><input type="text" value="<?= $serpKey ?>" name="ask_serp_settings[serp_key]"/></td>
        </tr>
        <tr>
            <th scope="row">Default Location</th>
            <td>
                <input type="text" value="<?= $serpLocation ?>" name="ask_serp_settings[serp_location]" placeholder="Stockport, United Kingdom"/>
            </td>
        </tr>
        <tr>
            <th scope="row">Device</th>
            <td>
                <select name="ask_serp_settings[serp_device]">
                    <option value="desktop" <?= $serpDevice === 'desktop' ? 'selected' : '' ?>>Desktop</option>
                    <option value="mobile" <?= $serpDevice === 'mobile' ? 'selected' : '' ?>>Mobile</option>
                    <option value="tablet" <?= $serpDevice === 'tablet' ? 'selected' : '' ?>>Tablet</option>
                </select>
            </td>
        </tr>
        <tr>
            <th scope="row">Google Domain</th>
            <td>
                <select name="ask_serp_settings[serp_domain]">
                    <option value="google.co.uk" <?= $serpDomain === 'google.co.uk' ? 'selected' : '' ?>>UK - google.co.uk</option>
                    <option value="google.com" <?= $serpDomain === 'google.com' ? 'selected' : '' ?>>US - google.com</option>
                </select>
            </td>
        </tr>
        <tr>
            <th scope="row">Google Country</th>
            <td>
                <select name="ask_serp_settings[serp_country]">
                    <option value="gb" <?= $serpCountry === 'gb' ? 'selected' : '' ?>>UK</option>
                    <option value="us" <?= $serpCountry === 'us' ? 'selected' : '' ?>>US</option>
                </select>
            </td>
        </tr>

        <tr>
            <th scope="row">Google Language</th>
            <td>
                <select name="ask_serp_settings[serp_language]">
                    <option value="en" <?= $serpLanguage === 'en' ? 'selected' : '' ?>>English</option>
                    <option value="de" <?= $serpLanguage === 'de' ? 'selected' : '' ?>>German</option>
                </select>
            </td>
        </tr>

        <tr>
            <th scope="row">Analyser on Pages</th>
            <td>

                <label class="switch">
                    <input type="checkbox" name="ask_serp_settings[serp_on_pages]" class="switch-input" <?= $serpPages ? 'checked' : ''?>>
                    <span class="switch-label" data-on="On" data-off="Off"></span>
                    <span class="switch-handle"></span>
                </label>
            </td>
        </tr>

        <tr>
            <th scope="row">Analyser on Posts</th>
            <td>
                <label class="switch">
                    <input type="checkbox" name="ask_serp_settings[serp_on_posts]" class="switch-input" <?= $serpPosts  ? 'checked' : ''?>>
                    <span class="switch-label" data-on="On" data-off="Off"></span>
                    <span class="switch-handle"></span>
                </label>
            </td>
        </tr>
        </tbody>
    </table>
        </div>
</div>
    <?php submit_button(); ?>
</form>