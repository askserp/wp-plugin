<?php
/*
Plugin Name:	AskSerp
Plugin URI:		https://askserp.com
Description:	Serp platform for content writing
Version:		1.0.12
Author:			Ask Serp
Author URI:		https://askserp.com
License:		GPL-2.0+
License URI:	http://www.gnu.org/licenses/gpl-2.0.txt

This plugin is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 2 of the License, or
any later version.

This plugin is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with This plugin. If not, see {URI to Plugin License}.
*/

if (!defined('WPINC')) {
    die;
}

require 'plugin-update-checker/plugin-update-checker.php';
$myUpdateChecker = Puc_v4_Factory::buildUpdateChecker(
    'https://gitlab.com/askserp/wp-plugin',
    __FILE__
);

//Set the branch that contains the stable release.
$myUpdateChecker->setBranch('master');

if(is_admin()) {
    include(plugin_dir_path(__FILE__) . 'admin/main.php');
    include(plugin_dir_path(__FILE__) . 'main.php');
}

?>
