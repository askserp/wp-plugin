    === Ask Serp ===
    Contributors: Michael Pardon & Matthew McMahon
    Tags: serp, content writing, seo
    Requires at least: 4.7
    Tested up to: 5.8
    Stable tag: 4.3
    Requires PHP: 7.4
    License: GPLv2 or later
    License URI: https://www.gnu.org/licenses/gpl-2.0.html

    AskSerp is a supportive blog / page creation platform that will scan the web to find the best types of keywords to rank in the best way possible via the search engines. This is a premium based plugin with a subscription service associated at https://askserp.com

    == Description ==

    AskSerp is a plugin that enables content writing in an SEO friendly manner with hints and tips to grow your SEO reach against competitors.

    == Changelog ==

    = 1.0.0 =
    * Creating the repo with the first base model of Ask Serp

    = 1.0.1 =
    * Testing auto updating

    = 1.0.2 =
    * CSS Design Updates

    = 1.0.3 =
    * Updated to work on new API framework with askserp.com

    = 1.0.4 =
    * New icon added to side navigation

    = 1.0.5 =
    * Icon designs added

    = 1.0.6 =
    * Adjusted paragraph & word count metrics

    = 1.0.7 =
    * Removal of configurable endpoint
    * Widen the block frame area
    * Adding Internal Links checking
    * Design changes to the component
    * Adding in new area for upcoming SEO Section

    = 1.0.8 =
    * Fixed issue where the saved version wasn't being brought forward in the editor.

    = 1.0.9 =
    * Updated to remove errors on fresh install
