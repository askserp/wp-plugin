<?php
add_action('add_meta_boxes', 'serpComponent');
$key = get_option('ask_serp_settings');

function serp_post_analyser() {
	$editor = use_block_editor_for_post($post) ? 'blocks' : 'classic';
    include(plugin_dir_path(__FILE__) . 'view/sidebar-component.php');
    wp_enqueue_script('custom-js', plugins_url('/assets/js/custom.js', __FILE__));
    wp_enqueue_style('custom-css', plugins_url('/assets/css/custom.css', __FILE__));
}

function serpComponent()
{
    $key = get_option('ask_serp_settings');
    if($key['serp_on_posts']) {
        add_meta_box(
            'ask-serp-analyser',
            'Ask Serp Analyser',
            'serp_post_analyser',
            'post',
            'side',
            'high'
        ); //Standard Posts
    }
    if($key['serp_on_pages']) {
        add_meta_box('ask-serp-analyser', 'Ask Serp Analyser', 'serp_post_analyser', 'page', 'side', 'high'); //Standard Pages
    }
    add_meta_box('ask-serp-analyser', 'Ask Serp Analyser', 'serp_post_analyser', 'blocks', 'side', 'high'); //Flatsome Blocks
}





add_action('save_post', 'saveSerp');

function saveSerp($id)
{
    //if (!empty($_POST['serp-post-data'])) {
        update_post_meta($id, 'askSerpPostJson', $_POST['serp-post-data']);
        update_post_meta($id, 'askSerpPostScore', $_POST['serp-post-score']);
        update_post_meta($id,'askSerpMetaTitle',$_POST['serp-meta-title']);
        update_post_meta($id,'askSerpMetaDescription',$_POST['serp-meta-description']);
    //}
}


add_filter( 'manage_posts_columns', 'serp_score_column_header' );
add_filter( 'manage_pages_columns', 'serp_score_column_header' );
add_filter( 'manage_blocks_columns', 'serp_score_column_header' );

add_action( 'manage_posts_custom_column', 'serp_score_column_record' ,10,2);
add_action( 'manage_pages_custom_column', 'serp_score_column_record' ,10,2);
add_action( 'manage_blocks_custom_column', 'serp_score_column_record' ,10,2);

function serp_score_column_header( $columns ) {
  $columns['serp-score'] = 'Serp Score';
  return $columns;
}



function serp_score_column_record( $column, $id ) {
	if ('serp-score' === $column ) {
	?>
<div>
	<?=get_post_meta($id,'askSerpPostScore')[0] ?? 'Unknown'?>
</div>
<?php 
	}
}
