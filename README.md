# AskSerp Plugin #

AskSerp is a supportive blog / page creation platform that will scan the web to find the best types of keywords to rank in the best way possible via the search engines. This is a premium based plugin with a subscription service associated at https://askserp.com

## Installation ##

1. Click on the `Download ZIP` button at the right to download the plugin.
2. Go to Plugins > Add New in your WordPress admin. Click on `Upload Plugin` and browse for the zip file.
3. Activate the plugin.

## Usage ##

1. Create an API Key over at https://askserp.com.
2. Update your settings with the relevant API Key
3. Create a post or page and start to write a title for your content, or update an existing page/post
4. Click the Run Serp button and let the magic unfold.
